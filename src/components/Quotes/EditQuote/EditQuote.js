import React, {Component} from 'react';
import axios from 'axios';
import Spinner from '../../UI/Spinner/Spinner';

class EditQuote extends Component {
  state = {
    quote: {}
  };

  componentDidMount() {
    axios.get(`/quotes/${this.props.location.id}.json`).then((response) => {
      console.log(response.data);

      this.setState({quote: response.data});
    });
  }

  authorNameChanged = event => {
    event.persist();

    this.setState(prevState => {
      return {
        quote: {...prevState.quote, [event.target.name]: event.target.value}
      }
    });
  };

  textChanged = event => {
    event.persist();

    this.setState(prevState => {
      return {
        quote: {...prevState.quote, [event.target.name]: event.target.value}
      }
    });
  };

  quoteEditHandler = (event) => {
    event.preventDefault();
    this.setState({isLoading: true});

    axios.put(`/quotes/${this.props.location.id}.json`,  this.state.quote).then(() => {
      this.setState({isLoading: false});
    })
  };


  render() {
    const isLoading = this.state.isLoading;
    console.log(this.state.quote);
    console.log(this.state.quote.author);

    return(
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Edit a quote</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-7">
            {isLoading && <div className="row justify-content-md-center">
              <div className="col-md-6">
                <Spinner />
              </div>
            </div>}
            <form>
              <div className="form-group">
                <label>Author</label>
                <input type="text"
                       className="form-control"
                       name="author"
                       value={this.state.quote.author}
                       onChange={this.authorNameChanged}
                />
              </div>
              <div className="form-group">
                <label>Quote text</label>
                <textarea className="form-control" rows="5"
                          name="text"
                          value={this.state.quote.text}
                          onChange={this.textChanged}
                />
              </div>
              <button type="submit" className="btn btn-primary mb-2" onClick={this.quoteEditHandler}>Save</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default EditQuote;