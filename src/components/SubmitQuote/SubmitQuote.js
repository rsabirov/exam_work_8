import React, {Component} from 'react';
import axios from 'axios';
import Spinner from '../UI/Spinner/Spinner';

class SubmitQuote extends Component {

  state = {
    quote:
      {
        author: '',
        category: '',
        text: ''
      },
    isLoading: false
  };

  authorNameChanged = event => {
    event.persist();

    this.setState(prevState => {
      return {
        quote: {...prevState.quote, [event.target.name]: event.target.value}
      }
    });
  };

  textChanged = event => {
    event.persist();

    this.setState(prevState => {
      return {
        quote: {...prevState.quote, [event.target.name]: event.target.value}
      }
    });
  };

  categoryChanged = event => {
    event.persist();

    this.setState(prevState => {
      return {
        quote: {...prevState.quote, [event.target.name]: event.target.value}
      }
    });
  };

  quoteCreateHandler = (event) => {
    event.preventDefault();
    this.setState({isLoading: true});

    axios.post('/quotes.json',  this.state.quote).then(() => {
      this.setState({isLoading: false});
    })
  };

  render() {

    console.log(this.state.quote.author);
    console.log(this.state.quote.category);
    console.log(this.state.quote.text);
    console.log(this.state.quote);

    const isLoading = this.state.isLoading;

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Submit a new quote</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-7">
            {isLoading && <div className="row justify-content-md-center">
              <div className="col-md-6">
                <Spinner />
              </div>
            </div>}
            <form>
              <div className="form-group">
                <label>Author</label>
                <input type="text"
                       className="form-control"
                       name="author"
                       value={this.state.quote.author}
                       onChange={this.authorNameChanged}
                />
              </div>
              <div className="form-group">
                <label>Category</label>
                <select className="form-control"
                        value={this.props.categories}
                        onChange={this.categoryChanged}
                        name="category"
                >
                  {
                    this.props.categories.map(category => {
                      return <option selected="selected" value={category.id} key={category.id}>{category.title}</option>
                    })
                  }
                </select>
              </div>
              <div className="form-group">
                <label>Quote text</label>
                <textarea className="form-control" rows="5"
                          name="text"
                          value={this.state.quote.text}
                          onChange={this.textChanged}
                />
              </div>
              <button type="submit" className="btn btn-primary mb-2" onClick={this.quoteCreateHandler}>Save</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default SubmitQuote;