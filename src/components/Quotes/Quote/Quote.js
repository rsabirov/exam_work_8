import React, {Component} from 'react';
import './quote.css';

class Quote extends Component {

  render() {

    // console.log(this.props.id);
    return (
      <div className="quote">
        <div className="row">
          <div className="col-md-9">
            <p>{this.props.text}</p>
            <span>-{this.props.author}</span>
          </div>
          <div className="col-md-3">
            <button className="btn btn-succes btn-block" onClick={() => this.props.edit(this.props.id)}>Edit</button>
            <button className="btn btn-primary btn-block" onClick={() => this.props.remove(this.props.id)}>Delete</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Quote;
