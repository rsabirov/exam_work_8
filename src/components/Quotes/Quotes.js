import React, {Component} from 'react';
import Quote from "./Quote/Quote";

class Quotes extends Component {

  editQuoteHandler = (id) => {
    this.props.history.push({
      pathname: `/edit/${id}`,
      id: id
    });
  };

  render() {
    console.log(this.props.quotes);
    const quotes = [];

    this.props.quotes.map(quote => {
        return quotes.push(
          <Quote author={quote.author}
                 category={quote.category}
                 text={quote.text}
                 id={quote.id}
                 key={quote.id}
                 remove={this.props.remove}
                 edit={this.editQuoteHandler}
          />)
    });
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <nav className="quotes-nav">
              <div className="list-group">
                {
                  this.props.categories.map(category => {
                    return <a href="#"
                              className="list-group-item list-group-item-action"
                              key={category.id}
                              onClick={() => this.props.getSelectedCategory(category.id, category.title)}
                    >{category.title}</a>
                  })
                }
              </div>
            </nav>
          </div>
          <div className="col-md-8">
            <div className="quotes-list">
              <div className="quotes-list__items">
                <h1>{this.props.activeCategory}</h1>
                {quotes}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Quotes;