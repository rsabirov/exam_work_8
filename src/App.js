import React, { Component,Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import axios from 'axios';

import Quotes from "./components/Quotes/Quotes";
import SubmitQuote from "./components/SubmitQuote/SubmitQuote";
import Header from "./shared/Header/Header";
import EditQuote from "./components/Quotes/EditQuote/EditQuote";

class App extends Component {

  state = {
    categories: [
      {title: 'All', id: 'all'},
      {title: 'Star Wars', id: 'star-wars'},
      {title: 'Famous people', id: 'famous-people'},
      {title: 'Saying', id: 'saying'},
      {title: 'Humour', id: 'Humour'},
      {title: 'Motivational', id: 'motivational'}
    ],
    activeCategory: '',
    quotes: []
  };

  // componentDidMount() {
  //   axios.get('/quotes.json').then((response) => {
  //
  //     const quotes = [];
  //
  //     for (let key in response.data) {
  //       quotes.push({...response.data[key], id: key});
  //     }
  //
  //     this.setState(prevState => {
  //       return {
  //         categories: [...prevState.categories], quotes: quotes
  //       }
  //     });
  //   });
  // }

  loadQuotesHandler = (id, activeTitle) => {

    console.log(id);

    if (id === 'all') {
      axios.get('/quotes.json').then((response) => {

        const quotes = [];

        for (let key in response.data) {
          quotes.push({...response.data[key], id: key});
        }

        this.setState(prevState => {
          return {
            categories: [...prevState.categories], quotes: quotes, activeCategory: activeTitle
          }
        });
      });
    } else {
      axios.get(`/quotes.json?orderBy="category"&equalTo="${id}"`).then((response) => {

        const quotes = [];

        for (let key in response.data) {
          quotes.push({...response.data[key], id: key});
        }

        this.setState(prevState => {
          return {
            categories: [...prevState.categories], quotes: quotes, activeCategory: activeTitle
          }
        });
      });
    }
  };

  quoteRemoveHandler = id => {

    axios.delete(`/quotes/${id}.json`).then(() => {
      this.setState(prevState => {
        const quotes = [...prevState.quotes];
        const index = quotes.findIndex(quote => quote.id === id);
        quotes.splice(index, 1);
        return {quotes};
      })
    })
  };

  render() {
    return (
        <Fragment>
          <Header/>
          <Switch>
            <Route path="/" exact render={(props) => (<Quotes
              categories={this.state.categories}
              quotes={this.state.quotes}
              remove={this.quoteRemoveHandler}
              getSelectedCategory={this.loadQuotesHandler}
              activeCategory={this.state.activeCategory}
              {...props}
            />)}  />
            <Route path="/submit-new-quote" render={(props) => (<SubmitQuote categories={this.state.categories} />)} />
            <Route path="/edit" component={EditQuote} />
          </Switch>
        </Fragment>
    );
  }
}

export default App;
