import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import './header.css';

class Header extends Component {
  render() {
    return (
        <header className="header">
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <div className="logo-box">
                  <a href="" className="logo-box__logo">Qoutes central</a>
                </div>
              </div>
              <div className="col-md-10">
                <div className="menu-box">
                  <nav className="site-menu">
                    <ul className="site-menu__list">
                      <li className="site-menu__item">
                        <NavLink exact to="/">Qoutes</NavLink>
                      </li>
                      <li className="site-menu__item">
                        <NavLink exact to="/submit-new-quote">Submit new qoute</NavLink>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </header>
    );
  }
}

export default Header;